package pl.sdacademy.shoutbox.view.messages;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.sdacademy.shoutbox.Constants;
import pl.sdacademy.shoutbox.R;
import pl.sdacademy.shoutbox.domain.Message;
import pl.sdacademy.shoutbox.domain.User;
import pl.sdacademy.shoutbox.service.ApiService;
import pl.sdacademy.shoutbox.service.PreferencesService;
import pl.sdacademy.shoutbox.view.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.enter_message)
    EditText messageEdit;

    private PreferencesService preferencesService;
    private ApiService apiService;

    private MessagesAdapter adapter;

    private IntentFilter intentFilter = new IntentFilter(Constants.MESSAGE_BROADCAST_ACTION);
    private PushMessageBroadcastReceiver broadcastReceiver;

    private String authorizationToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);

        preferencesService = new PreferencesService(this);
        apiService = new ApiService();

        User user = preferencesService.getUser();
        authorizationToken = preferencesService.getToken();

        broadcastReceiver = new PushMessageBroadcastReceiver();

        if (user == null || TextUtils.isEmpty(authorizationToken)) {
            // brak danych w zapisanych preferencjach
            logout();
        }

        setTitle(user);

        adapter = new MessagesAdapter(user);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);

        loadMessages();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                loadMessages();
                return true;
            case R.id.edit_profile:
                Toast.makeText(this, R.string.profile_edit, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.send_message)
    public void sendMessage() {
        String messageString = messageEdit.getText().toString();

        if (TextUtils.isEmpty(messageString)) {
            return;
        }

        Message message = new Message();
        message.setMessage(messageString);

        final ProgressDialog progressDialog = ProgressDialog.show(this, getString(R.string.message_sending), messageString);
        apiService.sendMessage(message, authorizationToken, new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                progressDialog.hide();
                if (!response.isSuccessful()) {
                    String errorMessage = getString(R.string.http_error, response.code());
                    Toast.makeText(MessagesActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                progressDialog.hide();
                Toast.makeText(MessagesActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        });

        messageEdit.getText().clear();
    }

    public void loadMessages() {
        apiService.getMessages(authorizationToken, new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if (!response.isSuccessful()) {
                    String errorMessage = getString(R.string.http_error, response.code());
                    Toast.makeText(MessagesActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    return;
                }

                adapter.setMessages(response.body());
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                Toast.makeText(MessagesActivity.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void logout() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.MESSAGES_TOPIC);
        preferencesService.clearData();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void setTitle(User user) {
        String fullName = user.getFirstName() + " " + user.getLastName();
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(fullName);
        }
    }

    private class PushMessageBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            loadMessages();
        }
    }
}
