package pl.sdacademy.shoutbox.view.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.sdacademy.shoutbox.R;
import pl.sdacademy.shoutbox.domain.User;
import pl.sdacademy.shoutbox.service.ApiService;
import pl.sdacademy.shoutbox.service.PreferencesService;
import pl.sdacademy.shoutbox.view.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements Callback<User> {

    @BindView(R.id.first_name)
    EditText firstNameEdit;

    @BindView(R.id.last_name)
    EditText lastNameEdit;

    @BindView(R.id.email)
    EditText emailEdit;

    @BindView(R.id.phone_number)
    EditText phoneEdit;

    @BindView(R.id.password)
    EditText passEdit;

    @BindView(R.id.password_confirm)
    EditText passConfirmEdit;

    private PreferencesService preferencesService;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        preferencesService = new PreferencesService(this);
        apiService = new ApiService();
    }

    @OnClick(R.id.button_register)
    public void register() {

        if (!isDataValid()) {
            Toast.makeText(this, R.string.invalid_form_data, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!isPasswordConfirmed()) {
            Toast.makeText(this, R.string.password_confirm_error, Toast.LENGTH_SHORT).show();
            return;
        }

        User user = createUser();
        apiService.registerUser(user, this);
    }

    private boolean isPasswordConfirmed() {
        return passEdit.getText().toString().equals(passConfirmEdit.getText().toString());
    }

    private boolean isDataValid() {
        return firstNameEdit.getText().length() >= 2
                && lastNameEdit.getText().length() >= 2
                && passEdit.getText().length() >= 6
                && isEmailValid(emailEdit.getText().toString());
    }

    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private User createUser() {
        User user = new User();
        user.setFirstName(firstNameEdit.getText().toString());
        user.setLastName(lastNameEdit.getText().toString());
        user.setEmail(emailEdit.getText().toString());
        user.setPhoneNumber(phoneEdit.getText().toString());
        user.setPassword(passEdit.getText().toString());
        return user;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {

        if (!response.isSuccessful()) {
            String message = getString(R.string.http_error, response.code());
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            return;
        }

        User user = response.body();
        if (user != null) {
            preferencesService.saveUser(user);
        }

        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
