package pl.sdacademy.shoutbox.view.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.sdacademy.shoutbox.Constants;
import pl.sdacademy.shoutbox.R;
import pl.sdacademy.shoutbox.view.register.RegisterActivity;
import pl.sdacademy.shoutbox.domain.User;
import pl.sdacademy.shoutbox.view.messages.MessagesActivity;
import pl.sdacademy.shoutbox.service.ApiService;
import pl.sdacademy.shoutbox.service.PreferencesService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements Callback<User> {

    @BindView(R.id.email)
    EditText emailEdit;

    @BindView(R.id.password)
    EditText passwordEdit;

    private PreferencesService preferencesService;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        preferencesService = new PreferencesService(this);
        apiService = new ApiService();

        User user = preferencesService.getUser();
        String token = preferencesService.getToken();

        if (user != null && !TextUtils.isEmpty(token)) {
            startMessagesActivity();
        } else if (user != null && !TextUtils.isEmpty(user.getEmail())) {
            emailEdit.setText(user.getEmail());
        }
    }

    @OnClick(R.id.button_login)
    public void login() {
        if (!isDataValid()) {
            Toast.makeText(this, R.string.invalid_form_data, Toast.LENGTH_SHORT).show();
            return;
        }

        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        apiService.login(email, password, this);
    }

    @OnClick(R.id.button_register)
    public void register() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        if (!response.isSuccessful()) {
            String message = getString(R.string.http_error, response.code());
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            return;
        }

        String token = response.headers().get(Constants.HEADER_AUTH_TOKEN);
        User user = response.body();

        if (token == null || user == null) {
            // we need to handle the situation when server responds with success code
            // but with no data
            return;
        }

        preferencesService.saveToken(token);
        preferencesService.saveUser(user);
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.MESSAGES_TOPIC);

        startMessagesActivity();
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private boolean isDataValid() {
        return isEmailValid(emailEdit.getText().toString())
                && passwordEdit.getText().length() >= 6;
    }

    private boolean isEmailValid(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void startMessagesActivity() {
        Intent messagesIntent = new Intent(this, MessagesActivity.class);
        startActivity(messagesIntent);
    }
}
