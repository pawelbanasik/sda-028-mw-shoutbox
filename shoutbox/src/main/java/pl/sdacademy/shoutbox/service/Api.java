package pl.sdacademy.shoutbox.service;

import java.util.List;

import pl.sdacademy.shoutbox.Constants;
import pl.sdacademy.shoutbox.domain.Message;
import pl.sdacademy.shoutbox.domain.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Api {

    @POST("login")
    Call<User> login(@Header(Constants.HEADER_AUTH_USERNAME) String email, @Header(Constants.HEADER_AUTH_PASSWORD) String password);

    @Headers(Constants.HEADER_CONTENT_TYPE_JSON)
    @POST("user")
    Call<User> register(@Body User user);

    @Headers(Constants.HEADER_CONTENT_TYPE_JSON)
    @PUT("user/{id}")
    Call<User> editDetails(@Path("id") Long id, @Body User user, @Header(Constants.HEADER_AUTH_TOKEN) String token);

    @DELETE("user/{id}")
    Call<Void> deleteUser(@Path("id") Long id, @Header(Constants.HEADER_AUTH_TOKEN) String token);

    @GET("message")
    Call<List<Message>> getMessages(@Header(Constants.HEADER_AUTH_TOKEN) String token);

    @Headers(Constants.HEADER_CONTENT_TYPE_JSON)
    @POST("message")
    Call<Message> createMessage(@Body Message message, @Header(Constants.HEADER_AUTH_TOKEN) String token);

    @Headers(Constants.HEADER_CONTENT_TYPE_JSON)
    @PUT("message/{id}")
    Call<Message> editMessage(@Path("id") Long id, @Body Message message, @Header(Constants.HEADER_AUTH_TOKEN) String token);

    @DELETE("message/{id}")
    Call<Void> deleteMessage(@Path("id") Long id, @Header(Constants.HEADER_AUTH_TOKEN) String token);
}
