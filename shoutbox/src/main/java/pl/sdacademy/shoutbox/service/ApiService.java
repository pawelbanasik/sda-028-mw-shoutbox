package pl.sdacademy.shoutbox.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import pl.sdacademy.shoutbox.Constants;
import pl.sdacademy.shoutbox.domain.Message;
import pl.sdacademy.shoutbox.domain.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {

    private Api api;

    public ApiService() {

        GsonBuilder builder = new GsonBuilder();

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
                Long milliseconds = json.getAsJsonPrimitive().getAsLong();
                return new Date(milliseconds);
            }
        });

        Gson gson = builder.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        api = retrofit.create(Api.class);
    }

    public void login(String email, String password, Callback<User> callback) {
        Call<User> loginCall = api.login(email, password);
        loginCall.enqueue(callback);
    }

    public void registerUser(User user, Callback<User> callback) {
        Call<User> registerCall = api.register(user);
        registerCall.enqueue(callback);
    }

    public void updateUser(User user, Callback<User> callback, String token) {
        Call<User> updateUserCall = api.editDetails(user.getId(), user, token);
        updateUserCall.enqueue(callback);
    }

    public void deleteUser(Long id, Callback<Void> callback, String token) {
        Call<Void> deleteUserCall = api.deleteUser(id, token);
        deleteUserCall.enqueue(callback);
    }

    public void getMessages(String token, Callback<List<Message>> callback) {
        Call<List<Message>> messagesCall = api.getMessages(token);
        messagesCall.enqueue(callback);
    }

    public void sendMessage(Message message, String token, Callback<Message> callback) {
        Call<Message> messageCall = api.createMessage(message, token);
        messageCall.enqueue(callback);
    }

    public void editMessage(Long id, Message message, String token, Callback<Message> callback) {
        Call<Message> messageCall = api.editMessage(id, message, token);
        messageCall.enqueue(callback);
    }

    public void deleteMessage(Long id, String token, Callback<Void> callback) {
        Call<Void> deleteMessageCall = api.deleteMessage(id, token);
        deleteMessageCall.enqueue(callback);
    }
}
