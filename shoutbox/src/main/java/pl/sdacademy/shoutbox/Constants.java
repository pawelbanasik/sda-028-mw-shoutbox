package pl.sdacademy.shoutbox;

public class Constants {
    public final static String API_BASE_URL = "http://178.62.27.214:8080/api/v1/";
    public final static String MESSAGE_BROADCAST_ACTION = "MESSAGE_BROADCAST_ACTION";
    public final static String MESSAGES_TOPIC = "messages";

    public final static String HEADER_AUTH_USERNAME = "Auth-Username";
    public final static String HEADER_AUTH_PASSWORD = "Auth-Password";
    public final static String HEADER_AUTH_TOKEN = "Auth-Token";
    public final static String HEADER_CONTENT_TYPE_JSON = "Content-Type: application/json";

    public final static String PATTERN_DATE_TIME = "dd.MM.yyyy HH:mm";
}
