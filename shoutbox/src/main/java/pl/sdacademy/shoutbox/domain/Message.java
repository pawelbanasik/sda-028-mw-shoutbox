package pl.sdacademy.shoutbox.domain;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private Long id;

    private String message;

    private User user;

    private Date createdDate;
}
